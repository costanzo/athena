# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

#! Note: Please check the TauJets_MuonRMCPContent and TauJets_EleRMCPContent if any new containers is added here. 

TauJetsCPContent = [
    "TauJets",
    "TauJetsAux.pt.eta.phi.m.ptFinalCalib.etaFinalCalib.ptTauEnergyScale.etaTauEnergyScale.charge.nChargedTracks.nIsolatedTracks.nAllTracks.isTauFlags.PanTau_DecayMode.NNDecayMode.NNDecayModeProb_1p0n.NNDecayModeProb_1p1n.NNDecayModeProb_1pXn.NNDecayModeProb_3p0n.RNNJetScore.RNNJetScoreSigTrans.RNNEleScore.RNNEleScoreSigTrans_v1.EleRNNLoose_v1.EleRNNMedium_v1.EleRNNTight_v1.tauTrackLinks.vertexLink.secondaryVertexLink.neutralPFOLinks.pi0PFOLinks.truthParticleLink.truthJetLink.trackWidth.centFrac.etOverPtLeadTrk.innerTrkAvgDist.absipSigLeadTrk.SumPtTrkFrac.EMPOverTrkSysP.ptRatioEflowApprox.mEflowApprox.dRmax.trFlightPathSig.massTrkSys.leadTrackProbNNorHT.EMFracFixed.etHotShotWinOverPtLeadTrk.hadLeakFracFixed.PSFrac.ClustersMeanCenterLambda.ClustersMeanFirstEngDens.ClustersMeanPresamplerFrac.GNTauScore_v0prune.GNTauScore_v1trunc.GNTauScoreSigTrans_v0prune.GNTauScoreSigTrans_v1trunc.GNTauVL_v0prune.GNTauL_v0prune.GNTauM_v0prune.GNTauT_v0prune.GNTauVL_v1trunc.GNTauL_v1trunc.GNTauM_v1trunc.GNTauT_v1trunc",
    "TauTracks",
    "TauTracksAux.pt.eta.phi.flagSet.trackLinks.rnn_chargedScore.rnn_isolationScore.rnn_conversionScore",
    "InDetTrackParticles",
    "InDetTrackParticlesAux.phi.vertexLink.theta.qOverP.truthParticleLink.truthMatchProbability",
    "TauSecondaryVertices",
    "TauSecondaryVerticesAux.x.y.z.covariance.trackParticleLinks",
    "TauNeutralParticleFlowObjects",
    "TauNeutralParticleFlowObjectsAux.pt.eta.phi.m",
    "TruthTaus",
    "TruthTausAux.pt_vis_neutral_pions.pt_vis_neutral_others.numCharged.classifierParticleType.classifierParticleOrigin.IsHadronicTau.numNeutralPion.numNeutral.numChargedPion.pt_vis.eta_vis.phi_vis.m_vis",
]

