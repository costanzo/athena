/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
/**
 * @file AthContainers/AuxTypeRegistry.icc
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2013
 * @brief Handle mappings between names and auxid_t.
 */


namespace SG {


/**
 * @brief Look up a name -> @c auxid_t mapping.
 * @param name The name of the aux data item.
 * @param clsname The name of its associated class.  May be blank.
 * @param flags Optional flags qualifying the type.  See above.
 * @param linkedVariable auxid of a linked variable, or null_auxid.
 *
 * The type of the item is given by the template parameter @c T,
 * and the @c ALLOC gives the type of the vector allocator.
 * If an item with the same name was previously requested
 * with a different type, then throw an @c AuxTypeMismatch exception.
 */
template <class T, class ALLOC>
SG::auxid_t
AuxTypeRegistry::getAuxID (const std::string& name,
                           const std::string& clsname /*= ""*/,
                           const Flags flags /*= Flags::None*/,
                           const SG::auxid_t linkedVariable /*= SG::null_auxid*/)
{
  return getAuxID (typeid(ALLOC), typeid(T),
                   name, clsname, flags, linkedVariable,
                   &AuxTypeRegistry::makeFactory<T, ALLOC>);
}


/**
 * @brief Verify type for an aux variable.
 * @param auxid The ID of the variable to check.
 * @param flags Optional flags qualifying the type.  See above.
 *
 * If the type of @c auxid is not compatible with the supplied
 * types @c T / @c ALLOC, then throw a @c SG::ExcAuxTypeMismatch exception.
 * Also may throw @c SG::ExcFlagMismatch.
 */
template <class T, class ALLOC>
void
AuxTypeRegistry::checkAuxID (const SG::auxid_t auxid,
                             const Flags flags /*= Flags::None*/)
{
  return checkAuxID (auxid, typeid(T), typeid(ALLOC), flags);
}


/**
 * @brief Copy elements between vectors.
 * @param auxid The aux data item being operated on.
 * @param dst Container for the destination vector.
 *            Declared as a rvalue reference to allow passing a temporary
 *            here (such as from AuxVectorInterface).
 * @param dst_index Index of the first destination element in the vector.
 * @param src Container for the source vector.
 * @param src_index Index of the first source element in the vector.
 * @param n Number of elements to copy.
 *
 * @c dst and @ src can be either the same or different.
 */
inline
void AuxTypeRegistry::copy (SG::auxid_t auxid,
                            AuxVectorData&& dst,      size_t dst_index,
                            const AuxVectorData& src, size_t src_index,
                            size_t n) const
{
  return copy (auxid, dst, dst_index, src, src_index, n);
}


/*
 * @brief Test whether this is a linked variable.
 */
inline
bool AuxTypeRegistry::isLinked (SG::auxid_t auxid) const
{
  return getFlags (auxid) & Flags::Linked;
}


/**
 * @brief Create an @c AuxTypeVectorFactory instance.
 *
 * This is passed to @c findAuxID when we're looking up an item
 * for which we know the type at compile-time.
 *
 * The ALLOC template parameter is the allocator to use
 * for the resulting vector.
 */
template <class T, class ALLOC>
inline
std::unique_ptr<IAuxTypeVectorFactory> AuxTypeRegistry::makeFactory() const
{
  return std::make_unique<SG::AuxTypeVectorFactory<T, ALLOC> >();
}


/**
 * @brief @c makeFactory implementation that always returns 0.
 *
 * This is passed to @c findAuxID when we're looking up an item
 * for which we do not know the type at compile-time.
 */
inline
std::unique_ptr<IAuxTypeVectorFactory> AuxTypeRegistry::makeFactoryNull() const
{
  return nullptr;
}


} // namespace SG
