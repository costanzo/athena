#define max_num_weights  50

extern "C" {
  void herwig7_veto_(int*);
  void herwiganalysis_();
  void herwig7_end_(int*);
  void herwig7_init_(int & maxev, const char* name, long int len);
  void herwig7_calculatescales_();
  double powheginput_(const char* , long int);

  //Declaration of fortran common blocks

  struct heprup {
    int idbmup[2];
    double ebmup[2];
    int pdfgup[2], pdfsup[2], idwtup, nprup;
    double xsecup[100], xerrup[100], xmaxup[100];
    int lprup[100];
  };
  extern heprup heprup_;

  struct hepeup {
    int nup, idprup;
    double xwgtup, scalup, aqedup, aqcdup;
    // nb. Fortran 2d arrays need to be declared as 1d C arrays
    //  in order to avoid -Wlto-type-mismatch warnings when compiling with LTO.
    int idup[500], istup[500], mothup[500*2], icolup[500*2];
    double pup[500*5], vtimup[500],spinup[500];
    int& getMother (unsigned ipart, unsigned imother) { return mothup[ipart*2+imother]; }
    int& getColor (unsigned ipart, unsigned icolor) { return icolup[ipart*2+icolor]; }
    double& getP (unsigned ipart, unsigned icomp) { return pup[ipart*5+icomp]; }
  };
  extern hepeup hepeup_;

  struct hepevt {
    // nb. Fortran 2d arrays need to be declared as 1d C arrays
    //  in order to avoid -Wlto-type-mismatch warnings when compiling with LTO.
    int nevhep, nhep, isthep[4000], idhep[4000], jmohep[4000*2],
      jdahep[4000*2];
    double phep[4000*5], vhep[4000*4];
  };
  extern hepevt hepevt_;

  struct weights {
    double weight[max_num_weights];
    int numweights;
    int radtype;
  };
  extern weights weights_;
  // the scales of the emissions from the resonances generated by POWHEG
  struct resonancevetos {
    double vetoscaletp,vetoscaletm, vetoscalewp,vetoscalewm;
  };
  extern resonancevetos resonancevetos_;

  struct c_innlodec {
    int innlodec;
  };
  extern c_innlodec c_innlodec_;

  extern struct{
     double btildecorr, remncorr;
  } corrfactors_;
    
}

