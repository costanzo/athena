/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "src/ITkPixelEncodingAlg.h"
#include "src/ITkPixelEncodingTool.h"
#include "src/ITkPixelHitSortingTool.h"

DECLARE_COMPONENT( ITkPixelEncodingAlg )
DECLARE_COMPONENT( ITkPixelEncodingTool )
DECLARE_COMPONENT( ITkPixelHitSortingTool)